# blur_wal_scripts_bspwm
----

I was preparing my next rice on BSPWM and I want to include some feature to be
more "distraction free" compliant. 
So as the Blurme scripts was to heavy I found a solution with x11 event catcher provide by BSPWM.

Then someone release a better python scripts to do it.

But whatever this is my simple solution, may you find what you need !

Inspired by:
  - [BlurWal](https://gitlab.com/BVollmerhaus/blurwal)
  - [blurme](https://github.com/ganifladi/blurme)

## How it work

You need:
	* ZSH - I tend to use only ZSH for my scripts to avoid issue between scripts I made and scrips I collected;
	* FEH - The Classy Wallpaper manager !
	* ImageMagick - CLI image modifier 

Please use the scripts named ```set_wallpaper``` to set your wallpaper, it will
create a folder in user home called ```.wallpaper```, in this folder it will
make every transition image for blur and a dimmed version of the wallpaper, that I
use with i3lock or my Display Manager.

then launch the scripts ```focus``` at start in background job.

I also provide two scripts to make the background transition, so you can bind it.
